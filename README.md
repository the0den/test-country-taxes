# Solution

## Refactoring

The refactored package lives in the `stringx` dir.

## Development

Project requires pyenv, poetry and docker (for MongoDB) to be installed locally.

- _MVC compliance_ is provided by the Django web framework;
- _REST API interface_ is supplied by the Django REST framework;
- The _composite pattern_ implementation stays in the `country_taxes.tax.domain` module,
  along with other parts of the _domain layer_;
- The _factory method_ pattern implementation lives in the `country_taxes.tax.service`,
  along with other parts of the _service layer_.

The commands to run:
```shell
# install the python requirements
poetry install

poetry shell
# Create superuser to log in in admin
country_taxes/manage.py createsuperuser
# Start local server
country_taxes/manage.py runserver
# Login in admin to have access to the API: visit http://127.0.0.1:8000/admin/

# Go to http://127.0.0.1:8000/api/, post some counties, states for a country.

# After checking work with sqlite RDBMS: lanch mongodb in a docker container.
./run-mongo.sh
# Switch USE_MONGO to `True` in `country_taxes/country_taxes/settings.py`
# to check the work of the NoSQL data source.
```


# Task

Refactoring
===========

Package stringUtils contains some code that has potential for optimisation. 
Refactor it in any way you think will make it better.


Development
===========

Develop a software that can be used to calculate statistics about the tax income of a country. 
The country is organized in 5 states and theses states are divided into counties.

Each county has a different tax rate and collects a different amount of taxes.

The software should have the following features:

- Output the overall amount of taxes collected per state
- Output the average amount of taxes collected per state
- Output the average county tax rate per state
- Output the average tax rate of the country 
- Output the collected overall taxes of the country


**Implementation expectations:**

- Application is built using MVC (or MVC-like) pattern with respects to S.O.L.I.D principles
- Application can use two different datasources of your choice (files vs RDBS,  NoSQL vs RDBS, etc.) and exact datasource can be switching using app configuration.
Please bear in mind that SQLite is a RDBS as well.
- Application should have REST API or CLI interface.