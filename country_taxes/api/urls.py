from rest_framework import routers
from rest_framework_mongoengine import routers as mongo_routers
from django.conf import settings

from tax.views import CountyViewSet, CountryViewSet, StateViewSet
from tax import mongoviews

router = routers.DefaultRouter()
router.register(r'counties', CountyViewSet)
router.register(r'states', StateViewSet)
router.register(r'countries', CountryViewSet)
urlpatterns = router.urls


if settings.USE_MONGO:
    router = mongo_routers.DefaultRouter()
    router.register(r'countries', mongoviews.CountryViewSet, 'country')
    urlpatterns = router.urls

