"""Contains in-memory domain models.

They know nothing about other layers (presentation, service, persistence).
"""

import statistics
from abc import ABC, abstractmethod
from typing import TypeVar, Generic, Iterable, Optional, List
from decimal import Decimal


class Taxable(ABC):
    """Interface for composite pattern.

    Allows to get the computation results on any level of hierarchy.
    """

    @abstractmethod
    def mean_tax_rate_percent(self) -> Decimal:
        raise NotImplementedError

    @abstractmethod
    def mean_tax_amount(self) -> Decimal:
        raise NotImplementedError

    @abstractmethod
    def total_tax_amount(self) -> Decimal:
        raise NotImplementedError


T = TypeVar('T', 'State', 'Country')


class Region(Taxable, Generic[T]):
    """Generic class for a container component.

    Serves as a common denominator for State and Country entities.
    """

    def __init__(self, subregions: Optional[Iterable[T]]):
        self.__subregions: List[T] = list(subregions) if subregions else []

    def add(self, subregion: T):
        self.__subregions.append(subregion)

    def mean_tax_rate_percent(self) -> Decimal:
        return statistics.mean(sr.mean_tax_rate_percent() for sr in self.__subregions)

    def mean_tax_amount(self) -> Decimal:
        return statistics.mean(sr.mean_tax_amount() for sr in self.__subregions)

    def total_tax_amount(self) -> Decimal:
        return sum(sr.total_tax_amount() for sr in self.__subregions)


class County(Taxable):

    def __init__(self, tax_rate_percent: Decimal, tax_amount: Decimal):
        self.__tax_rate_percent = tax_rate_percent
        self.__tax_amount = tax_amount

    def mean_tax_rate_percent(self) -> Decimal:
        return self.__tax_rate_percent

    def mean_tax_amount(self) -> Decimal:
        return self.__tax_amount

    def total_tax_amount(self) -> Decimal:
        return self.__tax_amount


class State(Region):

    def __init__(self, counties: Optional[Iterable[County]]):
        super(State, self).__init__(counties)


class Country(Region):

    def __init__(self, states: Optional[Iterable[State]]):
        super(Country, self).__init__(states)
