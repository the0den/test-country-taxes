"""Django-specific DTOs.

Part of the persistence layer.
"""

from django.db import models


class Country(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class State(models.Model):
    country = models.ForeignKey(Country, on_delete=models.CASCADE, related_name='states')


class County(models.Model):
    state = models.ForeignKey(State, on_delete=models.CASCADE, related_name='counties')
    tax_rate_percent = models.DecimalField(max_digits=4, decimal_places=2)
    tax_amount = models.DecimalField(max_digits=22, decimal_places=2)
