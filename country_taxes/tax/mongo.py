"""MongoDB-specific DTOs.

Part of the persistence layer.
"""

from mongoengine import Document, StringField, EmbeddedDocument, fields


class County(EmbeddedDocument):
    tax_rate_percent = fields.DecimalField(precision=2)
    tax_amount = fields.DecimalField(precision=2)


class State(EmbeddedDocument):
    counties = fields.EmbeddedDocumentListField(County)


class Country(Document):
    name = StringField(max_length=50)
    states = fields.EmbeddedDocumentListField(State)
