from decimal import Decimal
from rest_framework_mongoengine import serializers as mongo_serializers
from rest_framework import serializers

from .service import MongoCountryCreator, MongoStateCreator
from .mongo import Country, State


class StateSerializer(mongo_serializers.EmbeddedDocumentSerializer):
    class Meta:
        model = State
        fields = '__all__'

    mean_tax_rate_percent = serializers.SerializerMethodField()
    mean_tax_amount = serializers.SerializerMethodField()
    total_tax_amount = serializers.SerializerMethodField()

    def get_mean_tax_rate_percent(self, state: State) -> Decimal:
        return MongoStateCreator(state).new_taxable().mean_tax_rate_percent()

    def get_mean_tax_amount(self, state: State) -> Decimal:
        return MongoStateCreator(state).new_taxable().mean_tax_amount()

    def get_total_tax_amount(self, state: State) -> Decimal:
        return MongoStateCreator(state).new_taxable().total_tax_amount()


class CountrySerializer(mongo_serializers.DocumentSerializer):
    class Meta:
        model = Country
        fields = '__all__'

    states = StateSerializer(many=True)

    mean_tax_rate_percent = serializers.SerializerMethodField()
    total_tax_amount = serializers.SerializerMethodField()

    def get_mean_tax_rate_percent(self, country: Country) -> Decimal:
        return MongoCountryCreator(country).new_taxable().mean_tax_rate_percent()

    def get_total_tax_amount(self, country: Country) -> Decimal:
        return MongoCountryCreator(country).new_taxable().total_tax_amount()
