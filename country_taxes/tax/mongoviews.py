from rest_framework_mongoengine import viewsets

from .mongo import Country
from .mongoserializers import CountrySerializer


class CountryViewSet(viewsets.ModelViewSet):
    serializer_class = CountrySerializer

    def get_queryset(self):
        return Country.objects.all()
