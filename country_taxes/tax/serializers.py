from decimal import Decimal
from rest_framework import serializers

from .service import DjangoStateCreator, DjangoCountryCreator
from .models import County, State, Country


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = '__all__'

    mean_tax_rate_percent = serializers.SerializerMethodField()
    total_tax_amount = serializers.SerializerMethodField()

    def get_mean_tax_rate_percent(self, country: Country) -> Decimal:
        return DjangoCountryCreator(country).new_taxable().mean_tax_rate_percent()

    def get_total_tax_amount(self, country: Country) -> Decimal:
        return DjangoCountryCreator(country).new_taxable().total_tax_amount()


class StateSerializer(serializers.ModelSerializer):
    class Meta:
        model = State
        fields = '__all__'

    mean_tax_rate_percent = serializers.SerializerMethodField()
    mean_tax_amount = serializers.SerializerMethodField()
    total_tax_amount = serializers.SerializerMethodField()

    def get_mean_tax_rate_percent(self, state: State) -> Decimal:
        return DjangoStateCreator(state).new_taxable().mean_tax_rate_percent()

    def get_mean_tax_amount(self, state: State) -> Decimal:
        return DjangoStateCreator(state).new_taxable().mean_tax_amount()

    def get_total_tax_amount(self, state: State) -> Decimal:
        return DjangoStateCreator(state).new_taxable().total_tax_amount()


class CountySerializer(serializers.ModelSerializer):
    class Meta:
        model = County
        fields = '__all__'
