"""Service layer module.

In addition to views.py and mongoviews.py."""
from abc import ABC, abstractmethod

from . import models, mongo
from .domain import Country, State, County, Taxable


class TaxableCreator(ABC):
    """Factory method interface.

    Constructs taxable from arbitrary source.
    """

    @abstractmethod
    def new_taxable(self) -> Taxable:
        raise NotImplementedError


class DjangoCountryCreator(TaxableCreator):

    def __init__(self, country: models.Country):
        self.__country = country

    def new_taxable(self) -> Taxable:
        return Country(DjangoStateCreator(s).new_taxable() for s in self.__country.states.all())


class DjangoStateCreator(TaxableCreator):

    def __init__(self, state: models.State):
        self.__state = state

    def new_taxable(self) -> Taxable:
        return State(new_county(c) for c in self.__state.counties.all())


def new_county(c: models.County) -> County:
    return County(c.tax_rate_percent, c.tax_amount)


class MongoCountryCreator(TaxableCreator):

    def __init__(self, country: mongo.Country):
        self.__country = country

    def new_taxable(self) -> Taxable:
        return Country(MongoStateCreator(s).new_taxable() for s in self.__country.states)


class MongoStateCreator(TaxableCreator):

    def __init__(self, state: mongo.State):
        self.__state = state

    def new_taxable(self) -> Taxable:
        return State(mongo_new_county(c) for c in self.__state.counties)


def mongo_new_county(c: mongo.County) -> County:
    return County(c.tax_rate_percent, c.tax_amount)
