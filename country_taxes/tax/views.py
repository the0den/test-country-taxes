from django.shortcuts import render
from rest_framework import viewsets

from .models import County, State, Country
from .serializers import CountySerializer, StateSerializer, CountrySerializer


class CountryViewSet(viewsets.ModelViewSet):
    serializer_class = CountrySerializer
    queryset = Country.objects.all()


class StateViewSet(viewsets.ModelViewSet):
    serializer_class = StateSerializer
    queryset = State.objects.all()


class CountyViewSet(viewsets.ModelViewSet):
    serializer_class = CountySerializer
    queryset = County.objects.all()


