"""stringx package contains useful extensions to work with strings.

The pronunciation is /strɪŋeks/. The "X" stands for EXtensions.

Modules:

- string_: Contains the most frequently used functions for strings.
  To make importing less verbose they are (re)exported directly by the package.
- hash: Helpers to hash arbitrary objects.
"""


from .string_ import upper, lower, concatenate, concat, replace_first, trim


# To allow shorthands like ``from stringx import upper``.
__all__ = (
    'upper', 'lower', 'concatenate', 'concat', 'replace_first', 'trim'
)