"""hash module contains hashing-related routines.

Functions:

- md5hash: Construct md5 hasher.
- md5: Hash any object to the md5 hash string.
- sha512: Hash any object to the sha512 hash string.
"""


__all__ = (
    'md5hash', 'md5', 'sha512'
)


import hashlib
from typing import Any, Callable


def md5hash():
    """Return new md5 hash object."""
    return hashlib.md5()


def md5(obj: Any) -> str:
    """Calc md5 hash of ``obj``.

    :param obj: An object to hash
    :return: hex digest of the ``obj`` string representation
    :rtype: str

    >>> md5('test')
    '098f6bcd4621d373cade4e832627b4f6'
    """
    return __hexdigest(obj, hashlib.md5)


def sha512(obj: Any) -> str:
    """Calc sha512 hash of ``obj``.

    :param obj: An object to hash
    :return: hex digest of the ``obj`` string representation
    :rtype: str

    >>> sha512('test')
    'ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff'
    """
    return __hexdigest(obj, hashlib.sha512)


def __hexdigest(obj: Any, hash_factory: Callable[[bytes], Any]) -> str:
    return hash_factory(str(obj).encode('utf-8')).hexdigest()