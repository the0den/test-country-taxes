"""string_ module contains general string utility functions.

Functions:

- upper: Return uppercase version of a string.
- lower: Return lowercase version of a string.
- concatenate: Produce concatenated string representations of several objects.
- concat: Concatenate several strings.
- replace_first: Replace first occurrence of a substring.
- trim: Clear whitespace on the string sides.
"""


__all__ = (
    'upper', 'lower', 'concatenate', 'concat', 'replace_first', 'trim'
)


from typing import Any


def upper(s: str) -> str:
    """Return uppercase ``s``.

    :param str s: A string to uppercase
    :return: uppercase ``s``
    :rtype: str

    >>> upper('lOw0')
    'LOW0'
    """
    return s.upper()


def lower(s: str) -> str:
    """Return lowercase ``s``.

    :param str s: A string to lowercase
    :return: lowercase ``s``
    :rtype: str

    >>> lower('UPPeR9')
    'upper9'
    """
    return s.lower()


def concatenate(*objs: Any) -> str:
    """Concatenate string representations of ``objs``.

    :param tuple objs: Tuple of objects to concatenate
    :return: concatenated strings made from ``objs``

    >>> concatenate(1, False, 'obj', ())
    '1Falseobj()'
    """
    return concat(*map(str, objs))


def concat(*strings: str) -> str:
    """Concatenate ``strings``.

    :param tuple[str] strings: Tuple of strings to concatenate
    :return: concatenated ``strings``
    :rtype: str

    >>> concat('foo', 'bar')
    'foobar'
    """
    return ''.join(strings)


def replace_first(s: str, old: str, new: str) -> str:
    """Return ``s`` where the first ``old`` occurrence was changed to ``new``.

    :param str s: A string to replace in
    :param str old: A substring to be replaced
    :param str new: A substring to replace with
    :return: ``s`` with the ``old`` substring replaced once, if contained
    :rtype: str

    >>> replace_first('Hello World!', 'World', 'Mew')
    'Hello Mew!'
    """
    return s.replace(old, new, 1)


def trim(s: str) -> str:
    """Remove whitespace on the ``s`` sides.

    :param str s: A string to trim whitespace from
    :return: ``s`` with with whitespace trimmed on both sides
    :rtype: str

    >>> trim('  in between      \\n')
    'in between'
    """
    return s.strip()


#fixme consider removing unused internal function
def __writeLn(a):
    print(a, '\n')
